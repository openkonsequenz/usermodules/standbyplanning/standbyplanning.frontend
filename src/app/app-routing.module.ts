/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogoutComponent } from '@core/components/logout/logout.component';
import { DashboardComponent } from '@reporting/components/dashboard/dashboard.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { RedirectComponent } from '@core/components/redirect/redirect.component';
import { AboutComponent } from '@core/components/about/about.component';

const routes: Routes = [
  {
    path: 'loggedout',
    component: LogoutComponent
  },
  {
    path: 'redirect',
    component: RedirectComponent
  },
  {
    path: 'about',
    component: AboutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
