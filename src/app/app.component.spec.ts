/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';

import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

class FakeRouter {
    navigate(commands: any[]) {
        return commands[0];
    }
}

class AuthenticationServiceMock {
    login() {
        return of(
            {
                ret: 'OK'
            }
        );
    }
}

import { Component } from '@angular/core';
import { AuthenticationService } from '@core/services/authentication.service';
import { MessageService } from 'primeng/components/common/messageservice';

export function MockComponent(options: Component): Component {
    const metadata: Component = {
        selector: options.selector,
        template: options.template || '',
        inputs: options.inputs,
        outputs: options.outputs,
    };
    return Component(metadata)(<any>class MockClass { });
}

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let routerStub: FakeRouter;
    routerStub = {
        navigate: jasmine.createSpy('navigate').and.callThrough()
    };
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                MockComponent({ selector: 'ok-navigation' }),
                MockComponent({ selector: 'ok-version-info' }),
                MockComponent({ selector: 'ok-error' }),
                MockComponent({ selector: 'router-outlet' })
            ],
            providers: [
                { provide: Router, useValue: routerStub },
                { provide: APP_BASE_HREF, useValue: '/' },
                { provide: ActivatedRoute },
                { provide: HttpClient },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParams: of({ accessToken: 'test' })
                    }
                },
                {
                    provide: AuthenticationService,
                    useClass: AuthenticationServiceMock
                },
                MessageService
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
    });

    it('should create the app', async(() => {
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should create', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });
});
