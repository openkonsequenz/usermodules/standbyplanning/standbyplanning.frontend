/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthenticationService } from '@core/services/authentication.service';
import { ReturnObject } from '@shared/model/ReturnObject';

@Component({
  selector: 'ok-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private route$: Subscription;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private authService: AuthenticationService
  ) {

  }

  ngOnInit() {
    this.route$ = this.route.queryParams.subscribe(params => {
      if (params && params.accessToken) {
        this.authService.login(params.accessToken).subscribe((res: ReturnObject) => {
          if (res.ret === 'OK') {
            this.router.navigate(['/dashboard']);
          } else {
            this.router.navigate(['/loggedout']);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.route$) {
      this.route$.unsubscribe();
    }
  }
}
