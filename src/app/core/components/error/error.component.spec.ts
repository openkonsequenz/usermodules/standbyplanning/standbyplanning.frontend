/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ErrorComponent } from './error.component';
import { SharedModule } from '@shared/shared.module';
import { ErrorObject } from '@shared/model/ErrorObject';
import { MessageService } from 'primeng/components/common/messageservice';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorComponent],
      imports: [
        SharedModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display an error', () => {
    const error: ErrorObject[] = [{ httpStatus: 401, message: 'Unauthorized', localizedMessage: 'Unauthorized' }];
    component.utilService.error$.next(error);
    expect(component.errorMessage[0].httpStatus).toBe(401);
    expect(component.errorMessage[0].message).toBe('Unauthorized');
  });
});
