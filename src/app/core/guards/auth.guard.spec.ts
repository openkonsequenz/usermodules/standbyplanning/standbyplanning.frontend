/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuard } from '@core/guards/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('AuthGuard', () => {
  const router = {
    navigate: () => { }
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: Router, useValue: router }
      ],
      imports: [
        HttpClientModule,
        RouterTestingModule
      ]
    });
    localStorage.clear();
  });

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should return true if user is loggedin', inject([AuthGuard], (guard: AuthGuard) => {
    localStorage.setItem('ACCESS_TOKEN', 'test');
    const next: any = undefined;
    const state: any = undefined;
    const loggedIn = guard.canActivate(next, state);
    expect(loggedIn).toBeTruthy();
  }));

  it('should return false if user is not loggedin', inject([AuthGuard], (guard: AuthGuard) => {
    const next: any = undefined;
    const state: any = undefined;
    const loggedIn = guard.canActivate(next, state);
    expect(loggedIn).toBeFalsy();
  }));
});
