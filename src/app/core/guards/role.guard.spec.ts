/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { RoleGuard } from '@core/guards/role.guard';
import { Router } from '@angular/router';
import { AuthenticationService } from '@core/services/authentication.service';

describe('RoleGuard', () => {
  const router = {
    navigate: jasmine.createSpy('navigate')
  };
  const authService = {
    getUserRoles: () => {
      return ['BP_ADMIN'];
    }
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RoleGuard,
        { provide: Router, useValue: router },
        { provide: AuthenticationService, useValue: authService }
      ],
      imports: [HttpClientModule, RouterTestingModule]
    });
  });

  it('should ...', inject([RoleGuard], (guard: RoleGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should navigate to dashboard if roles are empty', inject([RoleGuard], (guard: RoleGuard) => {
    const next: any = {
      data: {
        expectedRoles: []
      }
    };
    const state: any = undefined;
    guard.canActivate(next, state);
    expect(router.navigate).toHaveBeenCalledWith(['/dashboard']);
  }));

  it('should not navigate to dashboard if roles are matching', inject([RoleGuard], (guard: RoleGuard) => {
    const next: any = {
      data: {
        expectedRoles: ['BP_ADMIN']
      }
    };
    const state: any = undefined;
    const canActivate = guard.canActivate(next, state);
    expect(canActivate).toBeTruthy();
  }));

  it('should not navigate to dashboard if roles are not defined', inject([RoleGuard], (guard: RoleGuard) => {
    const next: any = {};
    const state: any = undefined;
    const canActivate = guard.canActivate(next, state);
    expect(canActivate).toBeTruthy();
  }));
});
