/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ErrorInterceptor} from '@core/interceptors/error.interceptor';
import {HttpErrorResponse} from '@angular/common/http';

describe('ErrorInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorInterceptor],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([ErrorInterceptor], (service: ErrorInterceptor) => {
    expect(service).toBeTruthy();
  }));

  it('should call error handler with response from back end', inject([ErrorInterceptor], (service: ErrorInterceptor) => {
    const httpErrorResponse = new HttpErrorResponse({
      error: {
        httpStatus: 401,
        message: 'Unauthorized',
        localizedMessage: 'Unauthorized'
      }
    });
    service.handleError(httpErrorResponse);
  }));

  it('should call error handler without response from back end and replace undefined strings', inject([ErrorInterceptor],
    (service: ErrorInterceptor) => {
      const httpErrorResponse = new HttpErrorResponse({
        error: {
          httpStatus: undefined,
          message: undefined,
          localizedMessage: undefined
        }
      });
      service.handleError(httpErrorResponse);
    }));
});
