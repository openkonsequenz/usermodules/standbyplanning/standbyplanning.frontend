/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationService } from '@core/services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { Routes } from '@angular/router';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationService,
        RouterTestingModule
      ],
      imports: [HttpClientModule]
    });
  });

  it('should be created', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));

  describe('getUsername()', () => {
    it('should return the userName from localStorage if it exists', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('USER_NAME', 'Maximilian Ridder');
      expect(service.getUsername()).toBeTruthy('Maximilian Ridder');
    }));

    it('should return the userName "noUsername" if there is no Username in localstorage', inject([AuthenticationService],
      (service: AuthenticationService) => {
        localStorage.removeItem('USER_NAME');
        expect(service.getUsername()).toBe('noUsername');
      }));
  });

  it('should return true', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service.logout()).toBeTruthy();
  }));

  describe('getUserRoles()', () => {
    it('should return an array of Userroles', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      const userRoles = service.getUserRoles();
      expect(userRoles).toEqual(['Bearbeiter', 'Admin']);
    }));

    it('should return an empty array of Userroles if there are no roles in localstorage', inject([AuthenticationService],
      (service: AuthenticationService) => {
        localStorage.removeItem('ROLES');
        const userRoles = service.getUserRoles();
        expect(userRoles).toEqual([]);
      }));
  });

  it('should return true if there is an access token in localstorage', inject([AuthenticationService], (service: AuthenticationService) => {
    localStorage.setItem('ACCESS_TOKEN', 'TEST');
    expect(service.isLoggedIn()).toBeTruthy();
  }));

  /*it('should return true if there is an access token in localstorage', inject([AuthenticationService],
    (service: AuthenticationService) => {
      localStorage.removeItem('ACCESS_TOKEN');
      expect(service.isLoggedIn()).toBeTruthy();
    }));*/

  it('should set localstorage params on login', inject([AuthenticationService], (service: AuthenticationService) => {
    service.login(`eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiO
    iI5OGVhMDcxMi02YWE3LTQ1ZjMtYWM0My1jN2JhNzliOGMwMTgiLCJleHAiOjE1Mjk5MzEyNDcsIm5iZiI6MCwiaWF0IjoxNTI5OTMwOTQ3LCJpc3MiOiJodHRwOi8vbG9jYWxob
    3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiI2NDMyMDgyYi03ZDUxLTRjNzQtYmRlMC01YjNlZTU4OTBhOGMiL
    CJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiODU1NTczMjktZDNkYy00YTQ2LWI1NzUtMjQwMDNhZ
    mM1OTYxIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR
    3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsic3RhbmRie
    XBsYW5uaW5nIjp7InJvbGVzIjpbIkJQX0FkbWluIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb
    2ZpbGUiXX19LCJyb2xlcyI6Ilt1bWFfYXV0aG9yaXphdGlvbiwgcGxhbm5pbmctYWNjZXNzLCBCUF9HcnVwcGVubGVpdGVyLCBCUF9TYWNoYmVhcmJlaXRlciwgb2ZmbGluZV9hY
    2Nlc3MsIEJQX0FkbWluLCBCUF9MZXNlYmVyZWNodGlndGVdIiwibmFtZSI6ImJwX2FkbWluIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYnBfYWRtaW4iLCJmYW1pbHlfbmFtZSI6I
    mJwX2FkbWluIn0.PmsnW7lACLTyFiBm1FptC2NgZoz0Un4fmLDwAs-dBWrNc5tYRk-9A47tvb28GDNBZ2pZOoan0kTPQBkMsiLjp5GOYvTY55kxJhVY6_rql4PmjqhOmZVng8yRE
    sse2qcLztWjSlDXoZZ2ICIAsLLUcsBdCNIgi3atgLXdlQS6sLE9xmI_BwXjzFh8vP0EDY6sOrzlmbisklx7Kq4LxSQFL5CLG7pvRO2sQewtWKtVZZA6qFydHTcZFr3MD4QsoVKS0
    Vw2iR6jSp9i-YLIX8le8R4dlNe0kiM0MZiNNlKHCPQYzv_ZEmw6yzCA9IMsEKWM3cIFls_xeFgkZ-xIoEc1Eg`);
    expect(service.isLoggedIn()).toBeTruthy();
  }));

  describe('userHasRoles()', () => {
    it('should return true if expectedRoles match any in ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Bearbeiter'])).toEqual(true);
    }));

    it('should return true if expectedRoles match any in ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Bearbeiter', 'Admin'])).toEqual(true);
    }));

    it('should return true if expectedRoles match any in ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Admin'])).toEqual(true);
    }));

    it('should return false if expectedRoles doesnt match any ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Bearbeite'])).toEqual(false);
    }));

    it('should return false if expectedRoles doesnt match any ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Sachbearbeiter'])).toEqual(false);
    }));

    it('should return false if expectedRoles doesnt match any ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['Admins'])).toEqual(false);
    }));

    it('should return false if expectedRoles doesnt match any ROLES', inject([AuthenticationService], (service: AuthenticationService) => {
      localStorage.setItem('ROLES', 'Bearbeiter,Admin');
      expect(service.userHasRoles(['SachBearbeiter'])).toEqual(false);
    }));
  });
});
