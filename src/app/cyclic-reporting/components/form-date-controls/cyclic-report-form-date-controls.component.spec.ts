/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup} from '@angular/forms';
import {SharedModule} from '@shared/shared.module';
import {CyclicReportFormDateControlsComponent} from './cyclic-report-form-date-controls.component';

describe('CyclicReportFormDateControlsComponent', () => {

  let component: CyclicReportFormDateControlsComponent;
  let fixture: ComponentFixture<CyclicReportFormDateControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CyclicReportFormDateControlsComponent
      ],
      imports: [
        CommonModule,
        SharedModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclicReportFormDateControlsComponent);
    component = fixture.componentInstance;
    const key = 'test';
    component.form = new FormGroup(['DayOffset', 'WeekDay', 'Hour', 'Minute']
      .reduce((result, postfix) => ({...result, [key + postfix]: new FormControl()}), {}));
    component.key = key;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

});
