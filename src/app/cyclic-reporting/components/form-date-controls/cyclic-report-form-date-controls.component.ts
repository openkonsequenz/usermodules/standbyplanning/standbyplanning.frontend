/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SelectOptionObject} from '@shared/model/SelectOptionObject';

@Component({
  selector: 'ok-cyclic-report-form-date-controls',
  styleUrls: ['cyclic-report-form-date-controls.component.scss'],
  templateUrl: 'cyclic-report-form-date-controls.component.html'
})
export class CyclicReportFormDateControlsComponent {

  private static id = 0;

  @Input()
  public controlId = `CyclicReportFormTriggerControlsComponent-${CyclicReportFormDateControlsComponent.id++}`;

  @Input()
  public triggerMinuteStep = 1;

  @Input()
  public form: FormGroup;

  @Input()
  public key: string;

  @Input()
  public forDayOffset: boolean;

  @Input()
  public options: SelectOptionObject[] = [];

}
