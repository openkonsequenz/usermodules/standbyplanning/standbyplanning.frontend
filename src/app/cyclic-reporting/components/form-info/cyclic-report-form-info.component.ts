/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DatePipe} from '@angular/common';
import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {interval, Subscription} from 'rxjs';
import {CyclicReportingUtilService} from '../../services/cyclic-reporting-util.service';

@Component({
  selector: 'ok-cyclic-report-form-info',
  styleUrls: ['cyclic-report-form-info.component.scss'],
  templateUrl: 'cyclic-report-form-info.component.html',
  providers: [DatePipe]
})
export class CyclicReportFormInfoComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  public data: CyclicReportObject;

  @Input()
  public dateReplacementTokens: { [token: string]: string };

  @Input()
  public dateFormat = 'dd.MM.yyyy, HH:mm \'Uhr\'';

  public nextTriggerDate: Date;

  public nextValidFromDate: Date;

  public nextValidToDate: Date;

  public subject: string;

  public fileName: string;

  public emailTextLines: string[];

  public refreshInterval = interval(1000 * 30);

  private subscriptions: Subscription[] = [];

  public constructor(
    public datePipe: DatePipe,
    public cyclicReportingUtilService: CyclicReportingUtilService
  ) {

  }

  public ngOnInit() {
    this.subscriptions.push(this.refreshInterval.subscribe(() => this.refresh(new Date())));
  }

  public ngOnChanges(changes: SimpleChanges) {
    const keysToRefresh: Array<keyof CyclicReportFormInfoComponent> = [ 'data', 'dateReplacementTokens'];
    if (keysToRefresh.some((key) => changes[key] != null)) {
      this.refresh(new Date());
    }
  }

  public ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  public refresh(date: Date) {
    try {
      this.nextTriggerDate = undefined;
      this.nextValidFromDate = undefined;
      this.nextValidToDate = undefined;
      this.subject = '';
      this.fileName = '';

      if (this.data == null) {
        return;
      }

      this.nextTriggerDate = this.cyclicReportingUtilService.getNextTriggerDate(this.data, date);
      this.nextValidFromDate = this.cyclicReportingUtilService.moveDateByDays(this.nextTriggerDate,
        this.data.validFromDayOffset, this.data.validFromHour, this.data.validFromMinute);
      this.nextValidToDate = this.cyclicReportingUtilService.moveDateByDays(this.nextTriggerDate,
        this.data.validToDayOffset, this.data.validToHour, this.data.validToMinute);

      this.subject = this.replaceTokens(this.data.subject, this.nextTriggerDate);
      this.fileName = this.replaceTokens(this.data.fileNamePattern, this.nextTriggerDate);
      this.emailTextLines = this.replaceTokens(this.data.emailText, this.nextTriggerDate).split('\n');
    } catch (e) {
      console.error(e);
    }
  }

  public replaceTokens(value: string, date: Date) {
    const replacements = Object.entries({...this.dateReplacementTokens})
      .map(([token, dateFormat]) => ({token, value: date == null ? '' : this.datePipe.transform(date, dateFormat)}));
    return replacements.reduce((result, replacement) => {
      return result.replace(new RegExp(replacement.token, 'g'), replacement.value);
    }, typeof value !== 'string' ? '' : value);
  }

}
