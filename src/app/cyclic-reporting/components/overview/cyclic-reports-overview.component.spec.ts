/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {SharedModule} from '@shared/shared.module';
import {StandbylistObject} from '@shared/model/StandbylistObject';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {RowNode} from 'ag-grid-community';
import {of} from 'rxjs';
import {CyclicReportsOverviewComponent} from './cyclic-reports-overview.component';

function createMockData<T extends object>(data: Partial<T>): T {
  return (data == null ? {} : data) as T;
}

describe('CyclicReportsOverviewComponent', () => {

  let component: CyclicReportsOverviewComponent;
  let fixture: ComponentFixture<CyclicReportsOverviewComponent>;

  const standByListData: StandbylistObject[] = Array(42).fill(0)
    .map((_, id) => createMockData<StandbylistObject>({ id, title: 'QVL ' + id }));

  const reportData: CyclicReportObject[] = Array(100).fill(0)
    .map((_, id) => createMockData<CyclicReportObject>({
      id,
      name: 'Cyclic Report ' + id,
      fileNamePattern: '{Date}_{Time}_{Week}_' + id,
      subject: 'Subject ' + id,
      to: [],

      reportName: 'ReportName ' + id,
      printFormat: 'pdf',
      standByListId: id % 42,
      statusId: 2,

      triggerWeekDay: (id + 6) % 7 + 1,
      triggerHour: id % 24,
      triggerMinute: (id % 12) * 5,
      validFromDayOffset: - id % 7,
      validFromHour: (id + 1) % 24,
      validFromMinute: (id + 2) % 60,
      validToDayOffset: 1 + id % 7,
      validToHour: (id + 3) % 24,
      validToMinute: (id + 4) % 24
    }));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CyclicReportsOverviewComponent
      ],
      imports: [
        CommonModule,
        SharedModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclicReportsOverviewComponent);
    component = fixture.componentInstance;
    spyOn(component.masterdataService, 'getStandbyListSelection').and.returnValue(of(standByListData));
    spyOn(component.cyclicReportingService, 'getCyclicReports').and.returnValue(of(reportData));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should compare trigger dates', () => {
    const comparator = component.columnDefs.find((col) => col.colId === 'trigger').comparator;
    const nodeA = createMockData<RowNode>({ data: reportData[0] });
    const nodeB = createMockData<RowNode>({ data: reportData[1] });
    expect(comparator('', '', nodeA, nodeA)).toBe(0);
    expect(comparator('', '', nodeA, nodeB)).toBe(6);
    expect(comparator('', '', nodeB, nodeA)).toBe(-6);
    expect(comparator('', '', nodeB, nodeB)).toBe(0);
  });

  it('should sort table', async () => {
    expect(() => component.sort('trigger', 'desc')).not.toThrow();
    await fixture.whenStable();
  });

  it('should select entries', async () => {
    const navigateSpy = spyOn(component.router, 'navigate');
    navigateSpy.calls.reset();
    await component.selectEntry(null);
    await component.selectEntry(createMockData<CyclicReportObject>({}));
    expect(navigateSpy).not.toHaveBeenCalled();
    await component.selectEntry(createMockData<CyclicReportObject>({ id: 19 }));
    expect(navigateSpy).toHaveBeenCalledWith([19], { relativeTo: component.activatedRoute });
  });

  it('should format data', () => {
    component.fetch();
    expect(component.formatData().length).toBe(reportData.length);
    expect(component.formatData(null)).toEqual([]);
    expect(component.formatData([ null, null ])).toEqual([]);
  });

  it('should format list name', () => {
    component.fetch();
    expect(component.data[0].listName).toBe('QVL 0');
    expect(component.formatListName(null)).toEqual('');
    expect(component.formatListName(createMockData<CyclicReportObject>({ standByListId: 19 }))).toEqual(standByListData[19].title);
    expect(component.formatListName(createMockData<CyclicReportObject>({ standByListId: standByListData.length }))).toEqual('');
    component.standByList = null;
    expect(component.formatListName(createMockData<CyclicReportObject>({ standByListId: 19 }))).toEqual('');
  });

});
