/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DatePipe} from '@angular/common';
import {Component, OnDestroy, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MasterdataService} from '@masterdata/services/masterdata.service';
import {StandbylistObject} from '@shared/model/StandbylistObject';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {AGGRID_LOCALETEXT, DEFAULT_COL_DEF} from '@shared/utils/list.util';
import {AgGridNg2} from 'ag-grid-angular';
import {ColDef} from 'ag-grid-community';
import {Subscription} from 'rxjs';
import {CyclicReportingUtilService} from '../../services/cyclic-reporting-util.service';
import {CyclicReportingService} from '../../services/cyclic-reporting.service';

export interface CyclicReportTableEntry extends CyclicReportObject {
  trigger: string;
  listName: string;
}

@Component({
  selector: 'ok-cyclic-reports-overview',
  styleUrls: ['cyclic-reports-overview.component.scss'],
  templateUrl: 'cyclic-reports-overview.component.html',
  providers: [
    DatePipe
  ]
})
export class CyclicReportsOverviewComponent implements OnDestroy {

  public hourFormat = 'HH:mm';

  public defaultColDef = DEFAULT_COL_DEF;

  public columnDefs: ColDef[] = [
    {
      headerName: 'Name',
      field: 'name',
      colId: 'name'
    },
    {
      headerName: 'Planart',
      field: 'reportName',
      colId: 'reportName',
    },
    {
      headerName: 'Liste',
      field: 'listName',
      colId: 'listName',
    },
    {
      headerName: 'Auslösezeitpunkt',
      field: 'trigger',
      colId: 'trigger',
      comparator: (valueA, valueB, nodeA, nodeB, isInverted) => {
        return this.cyclicReportingUtilService.cyclicReportTriggerComparator(nodeA.data, nodeB.data);
      }
    }
  ];

  public localeText = AGGRID_LOCALETEXT;

  public data: CyclicReportTableEntry[];

  public standByList: StandbylistObject[] = [];

  private subscriptions: Subscription[] = [];

  @ViewChild(AgGridNg2)
  private grid: AgGridNg2;

  public constructor(
    public cyclicReportingService: CyclicReportingService,
    public cyclicReportingUtilService: CyclicReportingUtilService,
    public masterdataService: MasterdataService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public datePipe: DatePipe
  ) {

  }

  public ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  public onGridReady() {
    this.fetch();
    this.grid.api.sizeColumnsToFit();
    this.sort('trigger');
  }

  public sort(colId: string, sort = 'asc') {
    this.grid.api.setSortModel([{colId, sort}]);
  }

  public fetch() {
    this.subscriptions.push(...[
      this.cyclicReportingService.getCyclicReports().subscribe((data) => this.formatData(data)),
      this.masterdataService.getStandbyListSelection().subscribe((data) => {
        this.standByList = data;
        this.formatData();
      })
    ]);
  }

  public async selectEntry(report: CyclicReportObject) {
    const id = report != null ? report.id : undefined;
    if (id != null) {
      return this.router.navigate([id], { relativeTo: this.activatedRoute });
    }
  }

  public formatData(data: CyclicReportObject[] = this.data) {
    return this.data = (Array.isArray(data) ? data : [])
      .filter((entry) => entry != null)
      .map((entry) => {
        const nextTriggerDate = this.cyclicReportingUtilService.getNextTriggerDate(entry);
        const weekDayLabel = this.cyclicReportingUtilService.formatWeekDay(entry.triggerWeekDay);
        return {
          ...entry,
          trigger: this.datePipe.transform(nextTriggerDate, `'${weekDayLabel}', ${this.hourFormat}`),
          listName: this.formatListName(entry)
        };
      });
  }

  public formatListName(report: CyclicReportObject): string {
    const standByList = Array.isArray(this.standByList) ? this.standByList : [];
    const standByListId = report != null ? report.standByListId : undefined;
    const selectedList = standByList.find((entry) => entry.id === standByListId);
    return selectedList != null ? selectedList.title : '';
  }

}
