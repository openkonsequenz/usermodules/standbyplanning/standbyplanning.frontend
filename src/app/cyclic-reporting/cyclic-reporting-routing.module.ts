/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@core/guards/auth.guard';
import {RoleGuard} from '@core/guards/role.guard';
import {CyclicReportFormComponent} from './components/form/cyclic-report-form.component';
import {CyclicReportsOverviewComponent} from './components/overview/cyclic-reports-overview.component';

const routes: Routes = [
  {
    path: 'zyklische-reports',
    component: CyclicReportsOverviewComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Admin']
    }
  },
  {
    path: 'zyklische-reports/:id',
    component: CyclicReportFormComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Admin']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CyclicReportingRoutingModule {

}
