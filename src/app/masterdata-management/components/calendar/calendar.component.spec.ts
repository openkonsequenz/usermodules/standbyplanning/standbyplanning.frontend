/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Routes, ActivatedRoute, Router } from '@angular/router';

import { CalendarComponent } from './calendar.component';
import { of } from 'rxjs';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { CalendarMockObjects } from '@shared/testing/calendar';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';

const calendarMockObjects = new CalendarMockObjects;

export class MasterDataServiceMock {
  saveDate() {
    return of(calendarMockObjects.DATE_OBJECT);
  }
  getDate() {
    return of(calendarMockObjects.DATEARRAY_OBJECT);
  }
  deleteDate() {
    return of({});
  }
}

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [
        CalendarComponent
      ],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        { provide: Router, useValue: router },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveDate()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ id: 1, name: 'test', date: { dateIndex: { day: 1, month: 1, year: 2018 } } });
      expect(component.saveDate()).toBeTruthy();
    });
  });

  describe('setDefaultDate', () => {
    it('should set date to current day', () => {
      const date = new Date();
      component.setDefaultDate('dateIndex');
      expect(component.form.get('date').get('dateIndex').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });
  });

  describe('deleteDate', () => {
    it('should delete a date with given id', () => {
      component.form.patchValue({ id: 1 });
      component.deleteDate();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/kalender']);
    });
  });
});
