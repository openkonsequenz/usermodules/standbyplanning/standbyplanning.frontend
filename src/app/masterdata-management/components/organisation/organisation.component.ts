/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { OrganisationObject } from '@shared/model/OrganisationObject';
import { FormUtil } from '@shared/utils/form.util';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.scss']
})
export class OrganisationComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  param$: Subscription;
  organisation$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id && id !== 'new') {
        this.organisation$ = this.masterDataService.getOrganisation(id).subscribe(
          data => {
            this.form.patchValue(data);
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      orgaName: ['', Validators.required],
      address: this.fb.group({
        id: '',
        postcode: ['', Validators.required],
        community: ['', Validators.required],
        communitySuffix: '',
        street: ['', Validators.required],
        housenumber: ['', Validators.required],
        wgs84zone: '',
        latitude: '',
        longitude: ''
      })
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveOrganisation() {
    if (FormUtil.validate(this.form)) {
      const organisationToSave = this.form.getRawValue();
      this.masterDataService.saveOrganisation(organisationToSave).subscribe((res: OrganisationObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        this.router.navigate(['/stammdatenverwaltung/organisation']);
      });
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.organisation$) {
      this.organisation$.unsubscribe();
    }
  }
}
