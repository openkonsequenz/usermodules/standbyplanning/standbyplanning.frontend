/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import { SharedModule } from '@shared/shared.module';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { RegionMockObjects } from '@shared/testing/region';

import { RegionComponent } from '@masterdata/components/region/region.component';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';

const regionMockObjects = new RegionMockObjects;

export class MasterDataServiceMock {
  saveRegion() {
    return of(regionMockObjects.REGION_OBJECT);
  }
  getRegion() {
    return of(regionMockObjects.REGION_OBJECT);
  }
  getLocationData() {
    return of(regionMockObjects.LOCATION_ARRAY);
  }
  getLocationDataSelection() {
    return of(regionMockObjects.LOCATION_SELECTION_ARRAY);
  }
  getFunctionData() {
    return of(regionMockObjects.FUNCTION_SELECTION_ARRAY);
  }
  getFunctionDataSelection() {
    return of(regionMockObjects.FUNCTION_SELECTION_ARRAY);
  }
  addRegionLocation() {
    return of(regionMockObjects.REGION_ARRAY);
  }
  deleteRegionLocation() {
    return of(regionMockObjects.REGION_ARRAY);
  }
}

describe('RegionComponent', () => {
  let component: RegionComponent;
  let fixture: ComponentFixture<RegionComponent>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [
        RegionComponent
      ],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        { provide: Router, useValue: router },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveRegion()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ regionName: 'test' });
      expect(component.saveRegion()).toBeTruthy();
    });

    it('should save the form if the component is opened in a modal and navigate away', () => {
      component.form.patchValue({ regionName: 'test' });
      component.isModal = true;
      component.saveRegion();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/region', 1]);
    });
  });

  describe('close()', () => {
    it('should navigate away when the component is not a modal', () => {
      component.isModal = false;
      component.close();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/region']);
    });

    it('should dispatch an event with content "close" if it is a modal and close() is called', () => {
      component.isModal = true;
      component.modalAction.subscribe((res) => {
        expect(res).toBe('close');
      });
      component.close();
    });
  });

  describe('move functions userfunction', () => {
    it('should move userfunction from source to target', () => {
      component.moveToTargetLocation(regionMockObjects.LOCATION_ARRAY);
      expect(component.targetLocation.length).toBeGreaterThan(0);
    });

    it('should move userfunction from target to source', () => {
      component.moveToSourceLocation(regionMockObjects.LOCATION_ARRAY);
      expect(component.targetLocation.length).toBe(2);
    });
  });
});
