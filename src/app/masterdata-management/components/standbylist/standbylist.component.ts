/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FormUtil } from '@shared/utils/form.util';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';
import { CopyDataDialogComponent } from '@masterdata/selection-dialogs/copy-data-dialog/copy-data-dialog.component';

@Component({
  selector: 'ok-user',
  templateUrl: './standbylist.component.html',
  styleUrls: ['./standbylist.component.scss']
})
export class StandbylistComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  modalAction = new Subject<string>();
  isModal = false;
  instanceId: number;

  /**
   * StandbyGroup
   */
  sourceStandbygroup: Array<StandbygroupObject> = [];
  targetStandbygroup: Array<any> = [];
  standbygroupList: Array<StandbygroupObject>;
  standbygroupData$: Subscription;
  standbygroupModalRef: NgbModalRef;

  sourceColumnDefsStandbygroup = [
    { headerName: 'Titel', field: 'title' }
  ];
  targetColumnDefsStandbygroup = [
    { headerName: 'Position', field: 'position' },
    { headerName: 'Titel', field: 'standbyGroupName' }
  ];

  param$: Subscription;
  standbylist$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId) {
        this.standbylist$ = this.masterDataService.getStandbylist(this.instanceId).subscribe(
          data => {
            this.form.patchValue(data);
            this.targetStandbygroup = data.lsStandbyListHasStandbyGroup;
            this.getStandbygroupList();
          }
        );
      }
    });
  }

  getStandbygroupList(): void {
    this.standbygroupData$ = this.masterDataService.getStandbygroupSelection().subscribe((standbygroupRes: StandbygroupObject[]) => {
      this.sourceStandbygroup = FormUtil.getSubsetOfArray(standbygroupRes, this.targetStandbygroup, 'id', 'standbyGroupId');
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      title: ['', Validators.required],
      modificationDate: ''
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveStandbylist() {
    if (FormUtil.validate(this.form)) {
      const standbygroupToSave = this.form.getRawValue();
      this.masterDataService.saveStandbylist(standbygroupToSave).subscribe((res: StandbylistObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/bereitschaftsliste', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/bereitschaftsliste']);
        }
      });
      return true;
    }
    return false;
  }

  /**
   * MOVE FUNCTIONS Standbygroups
   */

  moveToTargetStandbygroup(dataToMove, changeData: boolean) {
    // mapping for backend only if data is transferred from source to target
    if (!changeData) {
      for (let i = 0; i < dataToMove.length; i++) {
        dataToMove[i].standbyGroupId = dataToMove[i].id;
        dataToMove[i].standbyGroupName = dataToMove[i].title;
        dataToMove[i].standbyListId = this.instanceId;
        delete dataToMove[i].title;
        delete dataToMove[i].id;
      }
    }
    this.masterDataService.addStandbylistStandbygroup(this.instanceId, dataToMove).subscribe((resStandbyGroups: StandbygroupObject[]) => {
      this.targetStandbygroup = resStandbyGroups;
      this.sourceStandbygroup = FormUtil.getSubsetOfArray(this.sourceStandbygroup, this.targetStandbygroup, 'id', 'standbyGroupId');

      this.messageService.add({ severity: 'success', summary: 'Bereitschaftsgruppen wurden gespeichert', detail: '' });
    });
  }

  changeTargetDataStandbygroup(dataToMove) {
    if (dataToMove.length === 1) {
      this.standbygroupModalRef = this.modalService.open(CopyDataDialogComponent, { size: 'lg' });
      this.standbygroupModalRef.componentInstance.form.patchValue(
        {
          position: dataToMove[0].position
        }
      );
      this.standbygroupModalRef.componentInstance.decision.subscribe(resStandbyGroupModal => {
        if (resStandbyGroupModal) {
          dataToMove[0].position = resStandbyGroupModal.position;
          this.moveToTargetStandbygroup(dataToMove, true);
        }
        this.standbygroupModalRef.close();
      });
    }
  }

  moveToSourceStandbygroup(dataToMove) {
    this.masterDataService.deleteStandbylistStandbygroup(this.instanceId, dataToMove).subscribe(
      (resStandbyGroups: StandbygroupObject[]) => {
        this.targetStandbygroup = resStandbyGroups;
        this.getStandbygroupList();

        this.messageService.add({ severity: 'success', summary: 'Bereitschaftsgruppen wurden gespeichert', detail: '' });
      });
  }

  close() {
    if (this.isModal) {
      this.modalAction.next('close');
    } else {
      this.router.navigate(['/stammdatenverwaltung/bereitschaftsliste']);
    }
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.standbylist$) {
      this.standbylist$.unsubscribe();
    }
  }
}
