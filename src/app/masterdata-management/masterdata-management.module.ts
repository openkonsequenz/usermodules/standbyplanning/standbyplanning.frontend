/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';

import { MasterdataManagementRoutingModule } from '@masterdata/masterdata-management-routing.module';
import { OverviewComponent } from '@masterdata/components/overview/overview.component';
import { UserComponent } from '@masterdata/components/user/user.component';
import { UserlistComponent } from '@masterdata/components/userlist/userlist.component';
import { LocationComponent } from '@masterdata/components/location/location.component';
import { LocationlistComponent } from '@masterdata/components/locationlist/locationlist.component';
import { RegionComponent } from '@masterdata/components/region/region.component';
import { RegionlistComponent } from '@masterdata/components/regionlist/regionlist.component';
import { FunctionComponent } from '@masterdata/components/function/function.component';
import { FunctionlistComponent } from '@masterdata/components/functionlist/functionlist.component';
import { StandbygroupComponent } from '@masterdata/components/standbygroup/standbygroup.component';
import { StandbygrouplistComponent } from '@masterdata/components/standbygrouplist/standbygrouplist.component';
import { ValidityDialogComponent } from '@masterdata/selection-dialogs/validity-dialog/validity-dialog.component';
import { OrganisationComponent } from '@masterdata/components/organisation/organisation.component';
import { OrganisationlistComponent } from '@masterdata/components/organisationlist/organisationlist.component';
import { StandbylistComponent } from '@masterdata/components/standbylist/standbylist.component';
import { StandbylisttableComponent } from '@masterdata/components/standbylisttable/standbylisttable.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { CalendarlistComponent } from './components/calendarlist/calendarlist.component';
import { LoosingDataDialogComponent } from './selection-dialogs/loosing-data-dialog/loosing-data-dialog.component';
import { CopyDialogComponent } from './selection-dialogs/copy-dialog/copy-dialog.component';
import { CopyDataDialogComponent } from './selection-dialogs/copy-data-dialog/copy-data-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MasterdataManagementRoutingModule
  ],
  declarations: [
    OverviewComponent,
    UserComponent,
    UserlistComponent,
    LocationComponent,
    LocationlistComponent,
    RegionComponent,
    RegionlistComponent,
    FunctionComponent,
    FunctionlistComponent,
    StandbygroupComponent,
    StandbygrouplistComponent,
    ValidityDialogComponent,
    OrganisationComponent,
    OrganisationlistComponent,
    StandbylistComponent,
    StandbylisttableComponent,
    CalendarComponent,
    CalendarlistComponent,
    LoosingDataDialogComponent,
    CopyDialogComponent,
    CopyDataDialogComponent
  ],
  entryComponents: [
    ValidityDialogComponent,
    LoosingDataDialogComponent,
    CopyDialogComponent,
    CopyDataDialogComponent
  ]
})
export class MasterdataManagementModule { }
