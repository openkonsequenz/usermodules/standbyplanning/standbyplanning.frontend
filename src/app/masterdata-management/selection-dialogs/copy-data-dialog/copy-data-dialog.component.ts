/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';

import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'ok-copy-data-dialog',
  templateUrl: './copy-data-dialog.component.html',
  styleUrls: ['./copy-data-dialog.component.scss']
})
export class CopyDataDialogComponent extends AbstractDialogComponent implements OnInit {
  form: FormGroup = this.fb.group({
    position: ''
  });
  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
  }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    super.decide(decision, dataToTransmit);
  }

}
