/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from '@reporting/components/overview/overview.component';
import { StandbyReportingComponent } from '@reporting/components/standby-reporting/standby-reporting.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'reports',
    component: OverviewComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'reports/bereitschaft',
    component: StandbyReportingComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule { }
