/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractDialogComponent } from './abstract-dialog.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormUtil } from '@shared/utils/form.util';

describe('AbstractDialogComponent', () => {
  let component: AbstractDialogComponent;
  let fixture: ComponentFixture<AbstractDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AbstractDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('test decision method', () => {
    it('should validate form fields if decision is true and form fields are invalid', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validFrom: new FormControl('', Validators.required),
          validTo: new FormControl('')
        })
      });
      const dataToTransmit = component.form.getRawValue();
      component.decide(true, dataToTransmit);
      expect(component.form.valid).toBeFalsy();
    });
    it('should emit falsy value if decision was false', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validFrom: new FormControl('test', Validators.required),
          validTo: new FormControl('')
        })
      });
      component.decision.subscribe((res) => {
        expect(res).toBeFalsy();
      });
      const dataToTransmit = component.form.getRawValue();
      component.decide(false, dataToTransmit);
    });
    it('should emit dataToTramsit if decision is true and form is valid', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validFrom: new FormControl('test', Validators.required),
          validTo: new FormControl('')
        })
      });
      component.decision.subscribe((res) => {
        expect(res).toBeTruthy();
      });
      component.form.patchValue({ validFrom: { day: 1, month: 1, year: 2018 }, validTo: { day: 1, month: 1, year: 2018 } });
      const dataToTransmit = component.form.getRawValue();
      component.decide(true, dataToTransmit);
    });
    it('should emit truthy value if decision was true and data is undefined', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validFrom: new FormControl('test', Validators.required),
          validTo: new FormControl('')
        })
      });
      component.decision.subscribe((res) => {
        expect(res).toBeTruthy();
      });
      const dataToTransmit = undefined;
      component.decide(true, dataToTransmit);
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validFrom: new FormControl('')
        })
      });
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      component.form = new FormGroup({
        date: new FormGroup({
          validTo: new FormControl('')
        })
      });
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });
});
