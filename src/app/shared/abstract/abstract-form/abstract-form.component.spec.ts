/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from 'primeng/components/common/messageservice';

import { AbstractFormComponent } from './abstract-form.component';
import { SharedModule } from '@shared/shared.module';

describe('AbstractFormComponent', () => {
  let component: AbstractFormComponent;
  let fixture: ComponentFixture<AbstractFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule,
        NgbModule.forRoot()
      ],
      providers: [MessageService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('canDeactivate', () => {
    it('should stay on page and raise a modal if the form is dirty', () => {
      const formGroup: FormGroup = new FormGroup({
        first: new FormControl('test', Validators.required),
        last: new FormControl(''),
      });
      component.form = formGroup;
      component.form.markAsDirty();
      expect(component.canDeactivate()).toBeTruthy();
    });

    it('should switch page if the form isn´t dirty', () => {
      const formGroup: FormGroup = new FormGroup({
        first: new FormControl('test', Validators.required),
        last: new FormControl(''),
      });
      component.form = formGroup;
      expect(component.canDeactivate()).toBeTruthy();
    });

    it('should listen for decision and send the correct decision', () => {
      const formGroup: FormGroup = new FormGroup({
        first: new FormControl('test', Validators.required),
        last: new FormControl(''),
      });
      component.form = formGroup;
      component.form.markAsDirty();
      component.canDeactivate();
      component.decision.subscribe((res) => {
        expect(res).toBeTruthy();
      });
      component.decisionModalRef.componentInstance.decision.next(true);
    });
  });
});
