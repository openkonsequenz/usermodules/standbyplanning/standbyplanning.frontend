/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PicklistReactiveComponent } from '@shared/components/picklist-reactive/picklist-reactive.component';
import { AgGridModule } from 'ag-grid-angular';
import { ErrorComponent } from '@shared/components/error/error.component';
import { GridApi } from 'ag-grid-community';
import { PicklistMockObjects } from '@shared/testing/picklist';
import { HttpClientModule } from '@angular/common/http';

const picklistMockObjects = new PicklistMockObjects;

describe('PicklistReactiveComponent', () => {
  let component: PicklistReactiveComponent;
  let fixture: ComponentFixture<PicklistReactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PicklistReactiveComponent,
        ErrorComponent
      ],
      imports: [
        AgGridModule.withComponents([]),
        ReactiveFormsModule,
        NgbModule.forRoot(),
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicklistReactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.gridApiSource = new GridApi();
    component.gridApiSource.sizeColumnsToFit = () => { };

    component.gridApiTarget = new GridApi();
    component.gridApiTarget.sizeColumnsToFit = () => { };

    component.gridApiSource.getSelectedRows = () => {
      return picklistMockObjects.SELECTED_ROWS_OBJECT_SOURCE_ARRAY;
    };
    component.gridApiTarget.getSelectedRows = () => {
      return picklistMockObjects.SELECTED_ROWS_OBJECT_TARGET_ARRAY;
    };
    component.sourceList = picklistMockObjects.OBJECT_ARRAY_SOURCE;
    component.targetList = picklistMockObjects.OBJECT_ARRAY_TARGET;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('move from source to target', () => {
    // GridApi does not exists at the time the test runs, timeout is one possible solution
    /*fit('should emit the selected Row Data', () => {
      setTimeout(() => {
        console.log(component.gridApiSource);
        component.moveToTarget(false);
      }, 500);
    });
    */
    /*it('should emit all Row Data', () => {
      component.moveToTarget(true);
    });*/
  });

  describe('move from target to source', () => {
    /*
    // GridApi does not exists
    it('should emit the selected Row Data', () => {
      component.selectedRowsSource = SELECTED_ROWS_OBJECT_TARGET_ARRAY;
      component.moveToSource(false);

    });
    it('should emit all Row Data', () => {
      component.moveToSource(true);
    });
    */
  });

  describe('use header actions Right', () => {
    it('should emit the selected Row Data', () => {
      component.headerActionTargetClickedRight.subscribe((selectedRows) => {
        expect(selectedRows.length).toBe(1);
      });
      component.headerActionTargetRight();
    });
    it('shouldn´t emit the selected Row Data if selectedRows is empty', () => {
      component.gridApiTarget.getSelectedRows = () => {
        return [];
      };
      component.headerActionTargetClickedRight.subscribe((selectedRows) => {
        expect(selectedRows.length).toBe(0);
      });
      component.headerActionTargetRight();
    });
  });

  describe('use header actions Left', () => {
    it('should emit the selected Row Data', () => {
      component.headerActionTargetClickedLeft.subscribe((selectedRows) => {
        expect(selectedRows.length).toBe(1);
      });
      component.headerActionTargetLeft();
    });
    it('shouldn´t emit the selected Row Data if selectedRows is empty', () => {
      component.gridApiTarget.getSelectedRows = () => {
        return [];
      };
      component.headerActionTargetClickedLeft.subscribe((selectedRows) => {
        expect(selectedRows.length).toBe(0);
      });
      component.headerActionTargetLeft();
    });
  });
});
