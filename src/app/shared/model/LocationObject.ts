/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PostcodeObject } from '@shared/model/PostcodeObject';
import { BranchObject } from '@shared/model/BranchObject';
import { RegionObject } from '@shared/model/RegionObject';

export class LocationObject {
    id: number;
    title: string;
    district: string;
    shorttext: string;
    community: string;
    lsPostcode: PostcodeObject[];
    lsLocationForBranches: BranchObject[];
    lsRegions: RegionObject[];
}
