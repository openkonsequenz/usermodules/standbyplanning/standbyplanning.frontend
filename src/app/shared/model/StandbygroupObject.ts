/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export class StandbygroupObject {
    id: number;
    title: string;
    validFrom: any;
    validTo: any;
    numberOfStandby: number;
    lsRegions: Array<any>;
    lsStandbyDurations: Array<any>;
    lsUserInStandbyGroups: Array<any>;
    lsBranches: Array<any>;
    lsUserFunction: Array<any>;
    lsIgnoredCalendarDays: Array<any>;
    modificationDate: any;
    note: string;
    extendStandbyTime: boolean;
}
