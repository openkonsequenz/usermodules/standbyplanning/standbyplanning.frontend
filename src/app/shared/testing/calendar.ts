/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class CalendarMockObjects {
    DATE_OBJECT = {
        id: 1,
        name: 'test',
        date: '2018-06-01'
    };

    DATEARRAY_OBJECT = [{
        id: 1,
        name: 'test',
        date: '2018-06-01'
    }, {
        id: 2,
        name: 'abc',
        date: '2015-06-01'
    }];

    CALENDAR_ARRAY = [undefined];
}
