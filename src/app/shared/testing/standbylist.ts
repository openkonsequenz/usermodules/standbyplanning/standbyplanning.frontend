/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { StandbygroupObject } from '@shared/model/StandbygroupObject';

export class StandbylistMockObjects {
    STANDBYLIST_OBJECT = {
        'id': 1,
        'title': '',
        'validFrom': '',
        'validTo': '',
        'modificationDate': '',
        'lsStandbyListHasStandbyGroup': [{
            'id': 1,
            'title': 'group1',
            'note': 'Test-Notiz',
            'numberOfStandby': 1,
            'validFrom': '',
            'validTo': '',
            'modificationDate': ''
        }]
    };

    STANDBYGROUPSELECTION_OBJECT_ARRAY: StandbygroupObject[] = [
        {
            'id': 1,
            'title': 'group1',
            'numberOfStandby': 1,
            'validFrom': '',
            'validTo': '',
            'modificationDate': '',
            'lsRegions': [],
            'lsBranches': [],
            'lsIgnoredCalendarDays': [],
            'lsStandbyDurations': [],
            'lsUserFunction': [],
            'lsUserInStandbyGroups': [],
            'note': '',
            'extendStandbyTime': false
        },
        {
            'id': 2,
            'title': 'group2',
            'numberOfStandby': 1,
            'validFrom': '',
            'validTo': '',
            'modificationDate': '',
            'lsRegions': [],
            'lsBranches': [],
            'lsIgnoredCalendarDays': [],
            'lsStandbyDurations': [],
            'lsUserFunction': [],
            'lsUserInStandbyGroups': [],
            'note': '',
            'extendStandbyTime': false
        },
        {
            'id': 3,
            'title': 'Sondergruppe',
            'numberOfStandby': 1,
            'validFrom': '',
            'validTo': '',
            'modificationDate': '',
            'lsRegions': [],
            'lsBranches': [],
            'lsIgnoredCalendarDays': [],
            'lsStandbyDurations': [],
            'lsUserFunction': [],
            'lsUserInStandbyGroups': [],
            'note': '',
            'extendStandbyTime': false
        }
    ];

    STANDBYGROUPSELECTION_OBJECT_ARRAY_ONE_ELEMENT = [
        {
            'id': 1,
            'title': 'group1',
            'numberOfStandby': 1,
            'validFrom': '',
            'validTo': '',
            'modificationDate': ''
        }
    ];

    STANDBYLIST_ARRAY = [{
        title: 'test'
    },
    {
        title: 'test2'
    }];
}
