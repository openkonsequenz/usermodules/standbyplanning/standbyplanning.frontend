/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { FormUtil } from '@shared/utils/form.util';

describe('FormUtil', () => {
    it('should validatedAllFormFields', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            lsDistrict: new FormControl('')
        });
        FormUtil.validate(formGroup);
        // assert that errors exist
        expect(formGroup.controls.functionName.errors).toBeTruthy();
    });

    it('should validate sub FormGroups', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            subFormGroup: new FormGroup({
                id: new FormControl('')
            })
        });
        FormUtil.validate(formGroup);
        // assert that errors exist
        expect(formGroup.controls.functionName.errors).toBeTruthy();
    });

    it('should validate all FormGroups', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            subFormGroup: new FormGroup({
                id: new FormControl('')
            })
        });
        FormUtil.validate(formGroup);
        // assert that errors exist
        expect(formGroup.controls.functionName.errors).toBeTruthy();
    });

    it('shouldn´t validate all FormArrays', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            subFormArray: new FormArray([])
        });
        FormUtil.validate(formGroup);
        // assert that errors exist
        expect(formGroup.controls.functionName.errors).toBeTruthy();
    });

    it('should mark the formgroup as pristine', () => {
        const formGroup: FormGroup = new FormGroup({
            id: new FormControl(''),
            functionName: new FormControl('', Validators.required)
        });
        formGroup.markAsTouched();
        FormUtil.markAsPristineAndUntouched(formGroup);
        expect(formGroup.touched).toBeFalsy();
    });
    it('should mark the formgroup as pristine', () => {
        const formGroup: FormGroup = new FormGroup({
            id: new FormControl(''),
            functionName: new FormControl('', Validators.required)
        });
        formGroup.markAsDirty();
        FormUtil.markAsPristineAndUntouched(formGroup);
        expect(formGroup.pristine).toBeTruthy();
    });

    it('should add 5 days to 31.12.2018 and expect 04.01.2019', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            date: new FormGroup({
                validTo: new FormControl('')
            })
        });
        const date = new Date('2018-12-31');
        FormUtil.setInitialDate(formGroup);
        FormUtil.setDefaultReportDate(formGroup, 'validTo', 5, date);
        expect(formGroup.get('date').get('validTo').value).toEqual({ day: 5, month: 1, year: 2019 });
    });

    it('should patch validFrom and validTo dates to 01.01.20xx and 31.12.20xx', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            date: new FormGroup({
                validFrom: new FormControl(''),
                validTo: new FormControl('')
            })
        });
        const date = new Date();
        FormUtil.setInitialDate(formGroup);
        expect(formGroup.get('date').get('validFrom').value).toEqual({ day: 1, month: 1, year: date.getFullYear() + 1 });
        expect(formGroup.get('date').get('validTo').value).toEqual({ day: 31, month: 12, year: date.getFullYear() + 1 });
    });

    it('should patch validTo date 31.12.20xx', () => {
        const formGroup: FormGroup = new FormGroup({
            functionName: new FormControl('', Validators.required),
            date: new FormGroup({
                validFrom: new FormControl(''),
                validTo: new FormControl('')
            })
        });
        const date = new Date();
        FormUtil.setDefaultEndOfNextYear(formGroup, 'validTo');
        expect(formGroup.get('date').get('validTo').value).toEqual({ day: 31, month: 12, year: date.getFullYear() + 1 });
    });

    describe('setDefaultDate', () => {
        it('should set validFrom and date values to the current date and validTo to current + 15 years', () => {
            const formGroup: FormGroup = new FormGroup({
                functionName: new FormControl('', Validators.required),
                date: new FormGroup({
                    validFrom: new FormControl(''),
                    validTo: new FormControl('')
                })
            });
            const date = new Date();

            FormUtil.setDefaultDate(formGroup, 'validFrom');
            FormUtil.setDefaultDate(formGroup, 'validTo');

            expect(formGroup.get('date').get('validFrom').value).toEqual({
                day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
            });
            expect(formGroup.get('date').get('validTo').value).toEqual({
                day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
            });
        });
    });

    describe('addZero()', () => {
        it('should add a leading zero for number 2', () => {
            expect(FormUtil.addZero(2)).toBe('02');
        });

        it('should add a leading zero for number 9', () => {
            expect(FormUtil.addZero(9)).toBe('09');
        });

        it('shouldn´t add a leading zero to number 20', () => {
            expect(FormUtil.addZero(20)).toBe('20');
        });

        it('should cut off the first two numbers', () => {
            expect(FormUtil.addZero(1234)).toBe('34');
        });
    });

    describe('test addTimeToDates method', () => {
        it('should add times to date and result in datestring', () => {
            const obj = {
                date: {
                    validFrom: { day: 1, month: 1, year: 2018 },
                    validTo: { day: 1, month: 1, year: 2018 },
                },
                validFromTime: { hour: 10, minute: 10 },
                validToTime: { hour: 12, minute: 10 },
            };

            FormUtil.formatDates(obj, ['validFrom', 'validTo']);
            FormUtil.addTimeToDates(obj);
            expect(obj).toBeTruthy();
        });
        it('should not change the object if a needed property is not available', () => {
            const obj = {
                validFrom: { day: 1, month: 1, year: 2018 },
                validToTime: { hour: 12, minute: 10 }
            };

            FormUtil.addTimeToDates(obj);
            expect(obj).toEqual(
                {
                    validFrom: { day: 1, month: 1, year: 2018 },
                    validToTime: { hour: 12, minute: 10 }
                }
            );
        });
    });
});
