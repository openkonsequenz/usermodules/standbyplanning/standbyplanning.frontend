/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NgbDateStringParserFormatter } from '@shared/utils/dateFormatter.util';
import { NgbTimeStruct, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export class FormUtil {
    static getSubsetOfArray(arrayToBeFiltered, filterArray, comparisonProperty1, comparisonProperty2) {
        return arrayToBeFiltered.filter((e1) => {
            return !(filterArray.some((e2) => {
                return e1[comparisonProperty1] === e2[comparisonProperty2];
            }));
        });
    }

    /**
     * Validates a given Formgroup
     *
     * @param formGroup Any Formgroup which should be validated. In general this should be the 'root'-Formgroup
     *                  in order to validate every Formcontrol and Formgroup in the hierarchy
     */
    private static validateAllFormFields(formGroup: FormGroup) {
        if (formGroup) {
            Object.keys(formGroup.controls).forEach(field => {
                const control = formGroup.get(field);
                if (control instanceof FormControl) {
                    control.markAsTouched({ onlySelf: true });
                } else if (control instanceof FormGroup) {
                    this.validateAllFormFields(control);
                }
            });
        } else {
            return true;
        }
        return formGroup.valid;
    }

    static markAsPristineAndUntouched(form: FormGroup) {
        form.markAsPristine();
        form.markAsUntouched();
    }

    static validate(form: FormGroup): boolean {
        const formIsValid = this.validateAllFormFields(form);
        if (!formIsValid) {
            alert('Validierungsfehler!');
        }
        return formIsValid;
    }

    static setDefaultDate(form: FormGroup, field: string) {
        const date = new Date();
        const datePipe = new DatePipe('en-US');
        const ngbDateFormatter = new NgbDateStringParserFormatter();

        if (field === 'validTo') {
            date.setFullYear(date.getFullYear() + 15);
        }
        form.get('date').get(field).setValue(ngbDateFormatter.parse(datePipe.transform(date, 'yyyy-MM-dd')));
    }

    static setDefaultReportDate(form: FormGroup, field: string, days: number, date: Date) {
        const incrementedDate = this.addDays(days, date);
        const datePipe = new DatePipe('en-US');
        const ngbDateFormatter = new NgbDateStringParserFormatter();

        form.get('date').get(field).setValue(ngbDateFormatter.parse(datePipe.transform(incrementedDate, 'yyyy-MM-dd')));
    }

    static addDays(days: number, dateToIncrement: Date): Date {
        const date = new Date(dateToIncrement);
        date.setDate(date.getDate() + days);
        return date;
    }

    static setInitialDate(form: FormGroup) {
        const date = new Date();
        form.patchValue({
            date: {
                validFrom: { day: 1, month: 1, year: date.getFullYear() + 1 },
                validTo: { day: 31, month: 12, year: date.getFullYear() + 1 }
            }
        });
    }

    static setDefaultEndOfNextYear(form: FormGroup, field: string) {
        const date = new Date();
        const patchObj = {};
        patchObj[field] = { day: 31, month: 12, year: date.getFullYear() + 1 };
        form.get('date').patchValue(patchObj);
    }

    static convertToNgbTime(dateString: string): NgbTimeStruct {
        const date = new Date(dateString);
        return {
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds()
        };
    }



    static formatDates(obj: Object, properties: Array<any>): void {
        for (let i = 0; i < properties.length; i++) {
            if (obj['date'].hasOwnProperty(properties[i])) {
                const date: NgbDateStruct = obj['date'][properties[i]];
                if (date && date.year && date.month && date.day) {
                    obj[properties[i]] = `${date.year}-${date.month}-${date.day}`;
                }
            }
        }
        delete obj['date'];
    }

    static dateToISOString(date: Date) {
        return (
            `${date.getFullYear()}-` +
            `${FormUtil.addZero(date.getMonth() + 1)}-` +
            `${FormUtil.addZero(date.getDate())}T` +
            `${FormUtil.addZero(date.getHours())}:` +
            `${FormUtil.addZero(date.getMinutes())}:` +
            `${FormUtil.addZero(date.getSeconds())}`
        );
    }

    static addZero(num: number) {
        return ('0' + num).slice(-2);
    }

    static addTimeToDates(obj) {
        if (obj.hasOwnProperty('validFrom') && obj.hasOwnProperty('validFromTime')) {
          obj.validFrom = `${obj.validFrom}T${FormUtil.addZero(obj.validFromTime.hour)}:${FormUtil.addZero(obj.validFromTime.minute)}:00`;
        }
        if (obj.hasOwnProperty('validTo') && obj.hasOwnProperty('validToTime')) {
          obj.validTo = `${obj.validTo}T${FormUtil.addZero(obj.validToTime.hour)}:${FormUtil.addZero(obj.validToTime.minute)}:00`;
        }
      }
}
