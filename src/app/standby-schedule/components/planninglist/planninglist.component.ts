/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Validators } from '@angular/forms';

import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MenuItem } from 'primeng/components/common/menuitem';

import { PlanningService } from '@schedule/services/planning.service';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { CalculateDialogComponent } from '@schedule/dialogs/calculate-dialog/calculate-dialog.component';
import { ModalTransferObject } from '@shared/model/ModalTransferObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { FormUtil } from '@shared/utils/form.util';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { SearchBodiesObject } from '@shared/model/SearchBodiesObject';
import { PlanBodyObject } from '@shared/model/PlanBodyObject';
import { ProtocolDialogComponent } from '@schedule/dialogs/protocol-dialog/protocol-dialog.component';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { ReplaceDialogComponent } from '@schedule/dialogs/replace-dialog/replace-dialog.component';
import { ReplaceUserObject, MoveUserObject } from '@shared/model/PlanActionsObject';
import { GroupBodyObject } from '@shared/model/GroupBodyObject';
import { MoveDialogComponent } from '@schedule/dialogs/move-dialog/move-dialog.component';
import { AuthenticationService } from '@core/services/authentication.service';
import { ArchiveDialogComponent } from '@schedule/dialogs/archive-dialog/archive-dialog.component';
import { DeleteDataDialogComponent } from '@schedule/dialogs/delete-data-dialog/delete-data-dialog.component';

@Component({
  selector: 'ok-planninglist',
  templateUrl: './planninglist.component.html',
  styleUrls: ['./planninglist.component.scss']
})
export class PlanninglistComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  standbylistSelectionData = [];
  planningData$: Subscription;
  modalCalculationRef: NgbModalRef;
  modalProtocolRef: NgbModalRef;
  deleteModalWarningRef: NgbModalRef;
  planStatusId: number;
  planningHeader: string;
  modalReplaceDialogRef: NgbModalRef;
  modalMoveDialogRef: NgbModalRef;
  archiveModalRef: NgbModalRef;

  formUtil = FormUtil;

  @ViewChild('menu')
  menu: ElementRef;

  currentStandby: GroupBodyObject;

  items: MenuItem[] = [
    {
      label: 'ersetzen', icon: 'fa fa-exchange',
      command: () => {
        this.openReplaceDialog();
      }
    }, {
      label: 'verschieben', icon: 'fa fa-hand-o-right',
      command: () => {
        this.openMoveDialog();
      }
    }
  ];

  itemsLeiter: MenuItem[] = [
    {
      label: 'ersetzen', icon: 'fa fa-exchange',
      command: () => {
        this.openReplaceDialog();
      }
    }
  ];


  standbyGroups: StandbygroupObject[];

  plan: PlanBodyObject;

  constructor(
    private planningService: PlanningService,
    private masterDataService: MasterdataService,
    private injector: Injector,
    public authService: AuthenticationService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getStatusId();
    this.createForm();
    FormUtil.setInitialDate(this.form);
    this.getStandbyListData();
  }

  getStatusId() {
    const url = this.router.url.split('/');
    if (url[2] === 'planebene') {
      this.planStatusId = 1;
      this.planningHeader = 'Plan-Ebene';
    } else if (url[2] === 'istebene') {
      this.planStatusId = 2;
      this.planningHeader = 'Ist-Ebene';
    }
  }

  createForm() {
    this.form = this.fb.group({
      standbyListId: ['', Validators.required],
      date: this.fb.group({
        validFrom: ['', [Validators.required]],
        validTo: ['', [Validators.required]]
      }, { validator: [DatepickerValidator.dateRangeTo('')] }
      )
    });
  }

  getStandbyListData() {
    this.masterDataService.getStandbyListDropdown().subscribe((standbylistRes: StandbylistObject[]) => {
      this.standbylistSelectionData = standbylistRes;
    });
  }

  /**
   * Filter/Archive Methods
   */

  filterBodies(searchData: SearchBodiesObject) {
    this.planningService.filterBodies(this.planStatusId, searchData).subscribe((searchResult: PlanBodyObject) => {
      this.plan = searchResult;
    });
  }

  archiveBodies(searchData: SearchBodiesObject) {
    this.archiveModalRef = this.modalService.open(ArchiveDialogComponent, { size: 'lg', backdrop: 'static' });
    this.archiveModalRef.componentInstance.decision.subscribe((res) => {
      if (res) {
        const transferObject = Object.assign(searchData, res);
        this.planningService.archivePlan(this.planStatusId, transferObject).subscribe(() => {
          this.messageService.add({ severity: 'success', summary: 'Daten wurden archiviert', detail: '' });
        });
      }
      this.archiveModalRef.close();
    });
  }

  prepareDataAndCall(action: string) {
    const searchData: SearchBodiesObject = this.form.getRawValue();
    FormUtil.formatDates(searchData, ['validFrom', 'validTo']);
    if (FormUtil.validate(this.form)) {
      if (action === 'search') {
        this.filterBodies(searchData);
      } else if (action === 'archive') {
        this.archiveBodies(searchData);
      }
    }
  }

  /**
   * Calculation Methods
   */

  openCalculationModal(standbyGroupId: number) {
    const currentForm = this.form.getRawValue();
    this.modalCalculationRef = this.modalService.open(CalculateDialogComponent, { size: 'lg', backdrop: 'static' });
    this.modalCalculationRef.componentInstance.form.patchValue({
      standbyListId: currentForm.standbyListId,
      standbyGroupId: standbyGroupId,
      date: {
        validFrom: currentForm.date.validFrom,
        validTo: currentForm.date.validTo
      }
    });
    const modalAction: Subscription = this.modalCalculationRef.componentInstance.modalAction.subscribe((res: ModalTransferObject) => {
      if (res.action === 'calculate') {
        this.startCalculation(res.data);
        this.modalCalculationRef.close();
        modalAction.unsubscribe();
      }
    });
  }

  startCalculation(planningToCalculate) {
    FormUtil.formatDates(planningToCalculate, ['validFrom', 'validTo']);
    this.planningService.startCalculation(planningToCalculate).subscribe((calculationRes: ProtocolObject) => {
      this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
      this.modalProtocolRef.componentInstance.protocol = calculationRes;
      this.prepareDataAndCall('search');
    });
  }

  /**
   * Validate Methods
   */

  validatePlan(standbyGroupId: number) {
    const currentForm = this.form.getRawValue();
    FormUtil.formatDates(currentForm, ['validFrom', 'validTo']);
    if (standbyGroupId) {
      currentForm.standbyGroupId = standbyGroupId;
      delete currentForm.standbyListId;
    } else if (!FormUtil.validate(this.form)) {
      return false;
    }
    this.planningService.validate(this.planStatusId, currentForm).subscribe((validationRes: ProtocolObject) => {
      this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
      this.modalProtocolRef.componentInstance.protocol = validationRes;
    });
  }

  /**
   * Validate Methods
   */

  deletePlan() {
    const currentForm = this.form.getRawValue();
    FormUtil.formatDates(currentForm, ['validFrom', 'validTo']);
    if (FormUtil.validate(this.form)) {
      this.deleteModalWarningRef = this.modalService.open(DeleteDataDialogComponent);
      this.deleteModalWarningRef.componentInstance.decision.subscribe(resUserModal => {
        if (resUserModal) {
          this.planningService.deletePlan(this.planStatusId, currentForm).subscribe(() => {
            this.prepareDataAndCall('search');
          });
        }
        this.deleteModalWarningRef.close();
      });
    }
  }

  /**
   * Replace/Move Methods
   */

  openReplaceDialog() {
    this.modalReplaceDialogRef = this.modalService.open(ReplaceDialogComponent, { size: 'lg', backdrop: 'static' });
    this.modalReplaceDialogRef.componentInstance.form.patchValue({
      statusId: this.planStatusId,
      standbyGroupId: this.currentStandby.standbyGroup.id,
      currentUserId: this.currentStandby.user.id,
      date: {
        validFrom: this.ngbDateParserFormatter.parse(this.currentStandby.validFrom),
        validTo: this.ngbDateParserFormatter.parse(this.currentStandby.validTo)
      },
      validFromTime: FormUtil.convertToNgbTime(this.currentStandby.validFrom),
      validToTime: FormUtil.convertToNgbTime(this.currentStandby.validTo)
    });

    this.modalReplaceDialogRef.componentInstance.decision.subscribe((data: ReplaceUserObject) => {
      if (data) {
        this.planningService.replaceUser(data).subscribe((replaceUserRes) => {
          if (replaceUserRes) {
            this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
            this.modalProtocolRef.componentInstance.protocol = replaceUserRes;
            this.prepareDataAndCall('search');
          }
        });
      }
      this.modalReplaceDialogRef.close();
    });
  }


  openMoveDialog() {
    this.modalMoveDialogRef = this.modalService.open(MoveDialogComponent, { size: 'lg', backdrop: 'static' });
    this.modalMoveDialogRef.componentInstance.standbyGroupList = this.plan.planHeader.listGroups;
    this.modalMoveDialogRef.componentInstance.form.patchValue({
      scheduleBodyId: this.currentStandby.id,
      newUserId: this.currentStandby.user.id,
      statusId: this.planStatusId,
      date: {
        validFrom: this.ngbDateParserFormatter.parse(this.currentStandby.validFrom),
        validTo: this.ngbDateParserFormatter.parse(this.currentStandby.validTo)
      },
      validFromTime: FormUtil.convertToNgbTime(this.currentStandby.validFrom),
      validToTime: FormUtil.convertToNgbTime(this.currentStandby.validTo)
    });

    this.modalMoveDialogRef.componentInstance.decision.subscribe((data: MoveUserObject) => {
      if (data) {
        this.planningService.moveUser(data).subscribe((moveUserRes) => {
          if (moveUserRes) {
            this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
            this.modalProtocolRef.componentInstance.protocol = moveUserRes;
            this.prepareDataAndCall('search');
          }
        });
      }
      this.modalMoveDialogRef.close();
    });
  }

  openContextMenu(event, standby: GroupBodyObject) {
    this.currentStandby = standby;
    (<any>this.menu).toggle(event);
  }

  /**
   * Util Methods
   */

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  setMenuItems() {
    if (this.authService.userHasRoles(['BP_Sachbearbeiter', 'BP_Admin'])) {
      return this.items;
    } else {
      return this.itemsLeiter;
    }
  }

  ngOnDestroy() {
    if (this.planningData$) {
      this.planningData$.unsubscribe();
    }
  }
}
