/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Injector, EventEmitter, Output } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { FormUtil } from '@shared/utils/form.util';
import { UserObject } from '@shared/model/UserObject';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';
import { UserDropdownObject } from '@shared/model/UserDropdownObject';

@Component({
  selector: 'ok-replace-dialog',
  templateUrl: './replace-dialog.component.html',
  styleUrls: ['./replace-dialog.component.scss']
})
export class ReplaceDialogComponent extends AbstractDialogComponent implements OnInit {
  userList: Array<UserDropdownObject>;

  form: FormGroup = this.fb.group({
    date: this.fb.group({
      validFrom: ['', Validators.required],
      validTo: ['', Validators.required],
    }, { validator: [DatepickerValidator.dateRangeTo('')] }),
    validFromTime: ['', Validators.required],
    validToTime: ['', Validators.required],
    statusId: '',
    standbyGroupId: '',
    currentUserId: '',
    newUserId: ''
  });
  constructor(
    private fb: FormBuilder,
    private masterDataService: MasterdataService
  ) {
    super();
  }

  ngOnInit() {
    this.getAllUserList();
  }

  getAllUserList() {
    this.masterDataService.getUserlistDropdown().subscribe((resUserList: UserDropdownObject[]) => {
      this.userList = resUserList;
    });
  }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    FormUtil.formatDates(dataToTransmit, ['validFrom', 'validTo']);
    FormUtil.addTimeToDates(dataToTransmit);
    delete dataToTransmit.validFromTime;
    delete dataToTransmit.validToTime;
    super.decide(decision, dataToTransmit);
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

}
