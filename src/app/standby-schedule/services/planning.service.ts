/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilService } from '@core/services/util.service';
import { SchedulePlanningObject } from '@shared/model/SchedulePlanningObject';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { SearchBodiesObject } from '@shared/model/SearchBodiesObject';
import { PlanBodyObject } from '@shared/model/PlanBodyObject';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { MoveUserObject, ReplaceUserObject } from '@shared/model/PlanActionsObject';
import { ArchiveObject } from '@shared/model/ArchiveObject';
import { SearchCurrentStandbyObject } from '@shared/model/SearchCurrentStandbyObject';
import { QueryBodiesObject } from '@shared/model/QueryBodiesObject';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  constructor(
    private http: HttpClient,
    private utilService: UtilService
  ) { }


  getPlanningData() {
    return this.http.get<SchedulePlanningObject[]>(`${this.utilService.readConfig('basePath')}/standbyschedule/planning/list`);
  }

  getPlanning(id) {
    return this.http.get<SchedulePlanningObject>(`${this.utilService.readConfig('basePath')}/standbyschedule/planning/${id}`);
  }

  savePlanning(planningObj: SchedulePlanningObject) {
    return this.http.put<SchedulePlanningObject>(`${this.utilService.readConfig('basePath')}/standbyschedule/planning/save`, planningObj);
  }

  addStandbylistPlanning(id: number, standbyListArray: StandbylistObject[]) {
    return this.http.put<StandbylistObject[]>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/planning/${id}/standbylist/save/list`,
      standbyListArray
    );
  }

  deleteStandbylistPlanning(id: number, standbyListArray: StandbylistObject[]) {
    return this.http.put<StandbylistObject[]>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/planning/${id}/standbylist/delete/list`, standbyListArray
    );
  }

  startCalculation(scheduleHeader: SchedulePlanningObject) {
    return this.http.put<ProtocolObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/planning/blueprint/calculate`, scheduleHeader
    );
  }

  filterBodies(id: number, searchData: SearchBodiesObject) {
    return this.http.put<PlanBodyObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/filter/bodies/statusId/${id}/zip`, searchData
    );
  }

  /**
   * Transfer Bodies
   */

  transferBodies(dataToTransfer: SearchBodiesObject) {
    return this.http.put<PlanBodyObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/copy/bodies`, dataToTransfer
    );
  }

  /**
   * Plan Actions
   */

  moveUser(data: MoveUserObject) {
    return this.http.put<MoveUserObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/planning/user/move`, data
    );
  }

  replaceUser(data: ReplaceUserObject) {
    return this.http.put<ReplaceUserObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/planning/user/replace`, data
    );
  }

  /**
   * Archive Plan
   */

  archivePlan(id: number, searchData: SearchBodiesObject) {
    return this.http.put<any>(
      `${this.utilService.readConfig('basePath')}/archive/standbylist/${id}`, searchData
    );
  }

  getArchiveData() {
    return this.http.get<ArchiveObject[]>(
      `${this.utilService.readConfig('basePath')}/archive/list`
    );
  }

  searchArchiveData(searchData: SearchCurrentStandbyObject) {
    return this.http.put<ArchiveObject[]>(
      `${this.utilService.readConfig('basePath')}/archive/search`, searchData
    );
  }

  getArchive(id: number) {
    return this.http.get<ArchiveObject>(
      `${this.utilService.readConfig('basePath')}/archive/${id}`
    );
  }

  importIntoPlanning(id: number) {
    return this.http.get<any>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/import/archive/${id}`
    );
  }

  /**
   * Validation
   */

  validate(id: number, validationObj: SchedulePlanningObject) {
    return this.http.put<any>(
      `${this.utilService.readConfig('basePath')}/validation/standbyschedule/status/${id}`, validationObj
    );
  }

  /**
   * Delete Plan
   */

  deletePlan(id: number, deletionObj: SearchBodiesObject) {
    return this.http.put<any>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/delete/bodies/${id}`, deletionObj
    );
  }

  searchCurrentStandby(searchData: SearchCurrentStandbyObject) {
    return this.http.put<QueryBodiesObject>(
      `${this.utilService.readConfig('basePath')}/standbyschedule/searchnow`, searchData
    );
  }
}
