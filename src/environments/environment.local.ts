/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

export const environment = {
  production: true,
  basePath: 'http://localhost:8280/spbe/webapi',
  loginPage: 'http://localhost:8280/portalFE'
};
